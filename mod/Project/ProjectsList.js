ACE.mod('ProjectsList', function(ace){
    ace.get('mod', 'mod/Project/ProjectItem.js');
    return ProjectsList

    function ProjectsList(cfg){
        let id = cfg.id || 'list',
        projectItems = cfg.projectItems || [],
        projectItemACI,
        Location = cfg.getLocation
        aci = {
            set: {
                dat: setDat
            },
            get: {
                dat: getDat
            }
        },
        ux = {
            id,
            aci,
            dom: iniDom(),
            ini: (m)=> {
                cfg.ini(m)
            }
        };
        
        return ux;

        function iniDom(){
            let dom = [{
                dom: [
                    {
                        typ: 'span',
                        lbl: 'Project Name'
                    },
                    {
                        typ: 'span',
                        lbl: 'Project Description'
                    },
                    {
                        typ: 'span',
                        lbl: 'Project Lead'
                    },
                    {
                        typ: 'span',
                        lbl: 'Status'
                    }
                ]
            }];
            projectItems.forEach((itm) =>
            dom.push({
                mod: 'ProjectItem',
                ini: (m)=>{
                    projectItemACI = m
                },
                itm,
                Location
            })
        );
            return dom;
        }

        function setDat(){

        }

        function getDat(){

        }
    }
})