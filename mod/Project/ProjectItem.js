ACE.mod('ProjectItem', function(ace){
    return ProjectItem

    function ProjectItem(cfg){
        let id = cfg.id,
        data = cfg.itm || [],
        aci = {
            set: {
                dat: setDat
            },
            get: {
                dat: getDat
            }
        },
        ux = {
            id,
            aci,
            dom: iniDom(),
            ini: (m)=> {
                cfg.ini(m)
            }
        };
        
        return ux;

        function iniDom(){
            let dom = [
                {
                    dom: [
                        {
                            typ: 'label',
                            lbl: data.title,
                            on: {
                                click: ()=>{
                                    alert(data.title)
                                    cfg.Location(data, 2)
                                }
                            }   
                        },
                        {
                            typ: 'span',
                            lbl: data.desc
                        },
                        {
                            typ: 'span',
                            lbl: data.status
                        },
                        {
                            typ: 'span',
                            lbl: data.teamLead
                        }
                    ]
                }
            ]
            return dom;
        }

        function setDat(){

        }

        function getDat(){

        }
    }
})