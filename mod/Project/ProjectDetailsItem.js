ACE.mod('ProjectDetailsItem', function(ace){
    return ProjectDetailsItem

    function ProjectDetailsItem(cfg){
        let id = cfg.id,
        aci = {
            set: {
                dat: setDat
            },
            get: {
                dat: getDat
            }
        },
        ux = {
            id,
            aci,
            dom: iniDom(),
            ini: (m)=> {
                cfg.ini(m)
            }
        };
        
        return ux;

        function iniDom(){
            let dom = [
                {
                    dom: [
                        {
                            typ: 'button',
                            lbl: 'Go Back',
                            on: {
                                click: ()=>{}
                            }
                        },
                        {
                            typ: 'button',
                            lbl: 'Project Board',
                            on: {
                                click: ()=>{}
                            }
                        }
                    ]
                },
                {
                    typ: 'span',
                    lbl: '',
                    ini: (m)=>(titleACI = m)
                },
                {
                    typ: 'span',
                    lbl: '',
                    ini: (m)=>(descACI = m)
                },
                {
                    typ: 'span',
                    lbl: '',
                    ini: (m)=>(statusACI = m)
                },
                {
                    typ: 'span',
                    lbl: '',
                    ini: (m)=>(leadACI = m)
                },
                {
                    typ: 'span',
                    lbl: '',
                    ini: (m)=>(membersACI = m)
                },
                {
                    typ: 'a',
                    lbl: '',
                    ini: (m)=>(repoACI = m)
                },
                {
                    typ: 'a',
                    lbl: '',
                    ini: (m)=>(docsACI = m)
                },
                {
                    typ: 'a',
                    lbl: '',
                    ini: (m)=>(sheetACI = m)
                },
                {
                    typ: 'span',
                    lbl: 'Created at: ',
                    dom: {
                        typ: 'span',
                        lbl: '',
                        ini: (m)=>(dateACI = m)
                    }
                }
            ]
            return dom;
        }

        function setDat(data){
            titleACI.set('lbl', data.title)
            descACI.set('lbl', data.desc)
            statusACI.set('lbl', data.status)
            leadACI.set('lbl', data.teamLead)
            membersACI.set('lbl', data.teamMembers)
            repoACI.set('lbl', data.repo)
            docsACI.set('lbl', data.docs)
            sheetACI.set('lbl', data.sheet)
            dateACI.set('lbl', data.createdAt)
        }

        function getDat(){

        }
    }
})