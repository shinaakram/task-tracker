ACE.mod('ProjectDetailsSection', function(ace){
    ace.get('mod', 'mod/Project/ProjectDetailsItem.js');
    return ProjectDetailsSection

    function ProjectDetailsSection(cfg){
        let id = cfg.id || 'details',
        projectItems = cfg.projectItems || [],
        aci = {
            set: {
                dat: setDat
            },
            get: {
                dat: getDat
            }
        },
        ux = {
            id,
            dom: iniDom(),
            ini: (m)=> {
                cfg.ini(m)
            },
            aci
        };
        
        return ux;

        function iniDom(){
            let dom = [
                {
                    typ: 'h1',
                    lbl: 'Project Details'
                },
                {
                    mod: 'ProjectDetailsItem',
                    ini: (m)=> {
                        projectItemACI = m
                    }
                }
            ]
            return dom;
        }

        function setDat(data){
            projectItemACI.set('dat', data)

        }
        function getDat(){

        }
    }
})