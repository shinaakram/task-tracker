ACE(function (ace) {
    var DOM = ace.get.v('dom');
    ace.get('mod', 'mod/SwapContent.js');
    ace.get('mod', 'mod/Project/ProjectsList.js')
    ace.get('mod', 'mod/Project/ProjectDetailsSection.js')
    var swap, projectDetails, projectsListACI, ProjectDetailsACI,
    projectItems = [
            {
                title: 'Project 1',
                desc: 'Dummy Description',
                status: 'To Do',
                teamLead: 'Shina Akram',
                teamMembers: ['Muhammad Ali', 'Paul', 'James', 'Shina'],
            },
            {
                title: 'Project 2',
                desc: 'Dummy Description',
                status: 'In Progress',
                teamLead: 'Muhammad Ali',
                teamMembers: ['Muhammad Ali', 'Paul', 'James', 'Shina'],
            },
            {
                title: 'Project 3',
                desc: 'Dummy Description',
                status: 'Done',
                teamLead: 'Paul',
                teamMembers: ['Muhammad Ali', 'Paul', 'James', 'Shina'],
            },
            {
                title: 'Project 4',
                desc: 'Dummy Description',
                status: 'Done',
                teamLead: 'James',
                teamMembers: ['Muhammad Ali', 'Paul', 'James', 'Shina'],
            },
            {
                title: 'Project 5',
                desc: 'Dummy Description',
                status: 'In Progress',
                teamLead: 'Jeff',
                teamMembers: ['Muhammad Ali', 'Paul', 'James', 'Shina'],
            },
        ];
    DOM({
        id: 'main-div',
        dom: [
            {
                mod: 'SwapContent',
                ini: (m)=>{
                    swap=m;
                },
                loc: 1,
                items: [
                    {
                        mod: 'ProjectsList',
                        projectItems,
                        getLocation,
                        ini: (m)=> projectsListACI = m
                    },
                    {
                        mod: 'ProjectDetailsSection',
                        Location: getLocation,
                        projectDetails,
                        ini: (m)=> ProjectDetailsACI = m
                    }   
                ]
            }
        ]
    })

    function getLocation(data, loc){
        projectDetails = data;
        swap.set('loc', loc)  
        console.log(data)
        ProjectDetailsACI.set('dat', projectDetails)  
    }

})
